package com.eshop.congobongo.jwt.resource;

import java.io.Serializable;

public class JwtTokenRequest implements Serializable {

	private static final long serialVersionUID = -5616176897013108345L;

	private String username;
	private String password;

	//eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJpbjI4bWludXRlcyIsImV4cCI6MTU3MzQxMDA1NCwiaWF0IjoxNTcyODA1MjU0fQ.ay-9WXAP2KVpR5lXTEp_fUvCASdNxS2bO3hY2vCCFI1fT579PaBcyGoqJ_49kvkvNm8sTfmJXZk7Rj2Ny-VgDQ

	public JwtTokenRequest() {
		super();
	}

	public JwtTokenRequest(String username, String password) {
		this.setUsername(username);
		this.setPassword(password);
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
