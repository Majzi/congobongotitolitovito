package com.eshop.congobongo.jwt;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.eshop.congobongo.controllers.ConfigurationController;
import com.eshop.congobongo.entity.ConfigUsers.UserInformations;

import java.util.ArrayList;
import java.util.List;

@Service
public class JwtInMemoryUserDetailsService implements UserDetailsService {

	ConfigurationController configurationController = new ConfigurationController();
	static List<JwtUserDetails> inMemoryUserList = new ArrayList<>();

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		UserInformations user = configurationController.getUserConfiguration(username);

		if (user == null) {
			throw new UsernameNotFoundException(String.format("USER_NOT_FOUND '%s'.", username));
		}

		inMemoryUserList.add(new JwtUserDetails(user.getId(), user.getUsername(), user.getPassword(), user.getRole()));

		return new JwtUserDetails(user.getId(), user.getUsername(), user.getPassword(), user.getRole());
	}
}
