package com.eshop.congobongo.jwt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.List;

public class UnencryptedPasswordEncoder implements PasswordEncoder {

    @Autowired
    static List<JwtUserDetails> inMemoryUserList = new ArrayList<>();

    @Override
    public String encode(CharSequence charSequence) {
        return charSequence.toString();
    }

    @Override
    public boolean matches(CharSequence charSequence, String s) {
        return s.equals(charSequence);
    }
}
