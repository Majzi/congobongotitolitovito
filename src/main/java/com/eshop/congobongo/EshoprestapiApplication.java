package com.eshop.congobongo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EshoprestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(EshoprestapiApplication.class, args);
	}
}
