package com.eshop.congobongo.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class FrontendController {

    @GetMapping("/")
    public ModelAndView index(ModelMap model) {
        return new ModelAndView("redirect:/index.html");
    }

    @GetMapping("/login")
    public ModelAndView login(ModelMap model) {
        return new ModelAndView("redirect:/index.html");
    }
}
